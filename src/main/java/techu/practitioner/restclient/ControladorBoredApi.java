package techu.practitioner.restclient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api/v1/aburrido")
public class ControladorBoredApi {

    @Value("${apis.bored.v1}")
    String boredApiUrl;

    static class Bored {
        public String activity;
        public String type;
        public int participants;
        public double price;
        public String link;
        public double accesibility;
    }

    @GetMapping
    public String obtenerActividadParaHacer() {
        // SINCRONICO: solicitud -> esperar -> respuesta -> trabajo.
        // ASINCRONICO/NO BLOQUEANTE: solicitud -> [trabajo -> espero] -> respuesta -> trabajando.

        final RestTemplate restTemplate = new RestTemplate();

        final Bored bored = restTemplate.getForObject(boredApiUrl, Bored.class);

        return String.format("%s: %s", bored.type, bored.activity);
    }

}