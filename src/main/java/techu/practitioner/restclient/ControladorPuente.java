package techu.practitioner.restclient;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@RestController
@RequestMapping("/api/v1/puente")
public class ControladorPuente {

    @Value("${apis.external.v1}")
    String urlApiExterna;

    @Autowired
    RestTemplate restTemplate;

    static class Proveedor {
        public String nombre;
        public String direccion;
    }

    @PostMapping
    public ResponseEntity agregarProveedor(
            @RequestParam String nombre,
            @RequestParam(required = false) String direccion
    ) {
        final Proveedor p = new Proveedor();
        p.nombre = nombre;
        p.direccion = direccion;

        final URI location = restTemplate.postForLocation(this.urlApiExterna, p);
        final Proveedor r = restTemplate.getForObject(location, Proveedor.class);

        return ResponseEntity.ok().location(location).body(p);
    }

    @GetMapping
    public List<Proveedor> obtenerProveedores() {

        final ResponseEntity<String> respuesta = restTemplate.getForEntity(this.urlApiExterna, String.class);

        System.out.println("RESPUESTA: " + respuesta.getBody());

        if(respuesta.getStatusCode().isError())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        // jackson - Leer/Escribir JSON
        final ObjectMapper mapper = new ObjectMapper();
        try {
            final JsonNode nodo = mapper.readTree(respuesta.getBody());
            final Iterator<JsonNode> it = nodo.get("_embedded").get("proveedorList").elements();
            final ArrayList<Proveedor> pp = new ArrayList<>();
            while(it.hasNext()) {
                final JsonNode jsonNode = it.next();
                final Proveedor p = new Proveedor();
                p.nombre = jsonNode.get("nombre").asText();
                p.direccion = jsonNode.get("nombre").asText();
                pp.add(p);
            }
            return pp;
        } catch(Exception x) {
            throw  new ResponseStatusException(HttpStatus.BAD_GATEWAY);
        }
    }
}